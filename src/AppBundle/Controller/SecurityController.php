<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\User;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = ($request->getSession()->get('yubikey_error')) ?
            array("messageKey" => $request->getSession()->get('yubikey_error')) :
            $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $request->getSession()->remove('yubikey_error');

        return $this->render(
            'security/login.html.twig',
            array(
                'title'         => 'Авторизация',
                'last_username' => $lastUsername,
                'error'         => $error,
                'show_sidebar'  => false,
            )
        );
    }
}
