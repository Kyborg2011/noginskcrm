<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\User;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Status;
use AppBundle\Entity\Cattegory;
use AppBundle\Entity\Applicant;
use AppBundle\Entity\Attachment;
use AppBundle\Form\OrderItemType;
use AppBundle\Form\ApplicantType;

class ApiController extends Controller
{
    /**
     * @Route("/orders/delete/{id}", name="api_order_remove", requirements={
     *      "id": "\d+"
     * })
     */
    public function orderRemoveAction(Request $request, OrderItem $orderItem)
    {
        $orderKernel = $this->get('app.order_api');
        $orderKernel->remove($orderItem->getId());

        return new JsonResponse(array(
            'message' => true
        ));
    }
}
