<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Doctrine\ORM\Tools\Pagination\Paginator;

use AppBundle\Entity\User;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Status;
use AppBundle\Entity\Cattegory;
use AppBundle\Entity\Applicant;
use AppBundle\Entity\Attachment;

use AppBundle\Form\OrderItemType;
use AppBundle\Form\ApplicantType;

class OrderItemController extends Controller
{
    /**
     * @Route("/", name="homepage", defaults={"page" = 1})
     * @Route("/orders", name="orders_list_main", defaults={"page" = 1})
     * @Route("/orders/", name="orders_list_main_filter", defaults={"page" = 1})
     * @Route("/orders/page-{page}", name="orders_list_with_page", defaults={"page" = 1}, requirements={
     *      "page": "\d+"
     * })
     */
    public function indexAction(Request $request, $page)
    {
        /*
         * Load page settings from parameters
         */
        $onPage = $this->getParameter('order_items_on_page');
        if ($request->query->get('on_page', 0)) {
            $onPage = $request->query->get('on_page');
        }
        $firstElementIndex = ($page - 1) * $onPage;
        $defaultSortingField = $this->getParameter('order_default_sorting_field');
        $defaultSortingOrder = $this->getParameter('order_default_sorting_order');

        $repository = $this->getDoctrine()
                           ->getRepository('AppBundle:OrderItem');
        $queryBuilder = $repository->createQueryBuilder('p')
                            ->orderBy('p.' . $defaultSortingField, $defaultSortingOrder);

        if ($request->query->get('category_id', 0)) {
            $queryBuilder->andWhere('p.categoryId IN (:category_id)')
                ->setParameter('category_id', $request->query->get('category_id', 0));
        }

        if ($request->query->get('status_id', 0)) {
            $queryBuilder->andWhere('p.statusId IN (:status_id)')
                ->setParameter('status_id', $request->query->get('status_id', 0));
        }

        if ($request->query->get('date_from', 0) && $request->query->get('date_to', 0)) {
            $startDate = \DateTime::createFromFormat("d.m.Y", $request->query->get('date_from', 0));
            $endDate = \DateTime::createFromFormat("d.m.Y", $request->query->get('date_to', 0));
            $queryBuilder->andWhere('p.accepted BETWEEN :startDate AND :endDate')
                ->setParameter('startDate', $startDate)
                ->setParameter('endDate', $endDate);
        }

        $query = $queryBuilder->getQuery();
        $query->setFirstResult($firstElementIndex)
              ->setMaxResults($onPage);

        $orders = new Paginator($query, $fetchJoinCollection = true);

        $filterData = $this->get('app.filter_orders');
        $filter = $filterData->show();

        return $this->render('order_item/index.html.twig', [
            'base_dir'      => realpath($this->getParameter('kernel.root_dir').'/..'),
            'title'         => 'Список заявок',
            'show_sidebar'  => true,
            'show_top_menu' => true,
            'orders'        => $orders,
            'count_all'     => count($orders),
            'on_page'       => $onPage,
            'pages_count'   => ceil(count($orders) / $onPage),
            'current_page'  => $page,
            'filter_data'   => $filter,
        ]);
    }

    /**
     * @Route("/orders/new", name="orders_new")
     * @ParamConverter("orderItem", class="AppBundle:OrderItem")
     * @Route("/orders/edit/{id}", name="orders_edit")
     */
    public function newAction(Request $request, OrderItem $orderItem = null)
    {
        if (!$orderItem) {
            $orderItem = new OrderItem();
        }

        $form = $this->createForm(OrderItemType::class, $orderItem);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $attachments = $orderItem->getAttachments();
            foreach ($attachments as $attachment) {
                $attachment->setOrderItem($orderItem);
                $em->persist($attachment);
            }
            $em->persist($orderItem);
            $em->flush();
            $em->flush();
            $success = true;
        }

        return $this->render('order_item/new.html.twig', array(
            'title'         => ($orderItem->getId()) ? 'Редактирование заявки' : 'Создание заявки',
            'show_sidebar'  => false,
            'show_top_menu' => true,
            'form'          => $form->createView(),
            'success'       => (isset($success) && $success) ? true : false,
            'orderItem'     => $orderItem,
        ));
    }
}
