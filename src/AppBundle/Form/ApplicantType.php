<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ApplicantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('holder', TextType::class, array (
                    'attr'     => array (
                        'class' => 'holder-field',
                        'placeholder' => 'ФИО заявителя / Название компании *',
                    ),
                ))
                ->add('phone', TextType::class, array (
                    'required' => false,
                    'attr'     => array (
                        'class' => 'phone-field',
                        'placeholder' => 'Телефон',
                    ),
                ))
                ->add('street', TextType::class, array (
                    'required' => false,
                    'attr'     => array (
                        'class' => 'street-field',
                        'placeholder' => 'Улица',
                    ),
                ))
                ->add('house', TextType::class, array (
                    'required' => false,
                    'attr'     => array (
                        'class' => 'house-field',
                        'placeholder' => 'Дом',
                    ),
                ))
                ->add('appartment', NumberType::class, array (
                    'required' => false,
                    'attr'     => array (
                        'class' => 'apt-field',
                        'placeholder' => 'Кв.',
                    ),
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Applicant'
        ));
    }
}
