<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\Category;
use AppBundle\Entity\Status;
use AppBundle\Entity\Attachment;
use AppBundle\Repository\CategoryRepository;

class OrderItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST')
            ->add('accepted', null, array(
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy HH:mm:ss',
            ))
            ->add('applicant', ApplicantType::class)
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'query_builder' => function (CategoryRepository $er) {
                    $qb = $er->createQueryBuilder('c');
                    $sqb = $er->createQueryBuilder('r');
                    $db2 = $sqb->select('r.id')
                        ->where('r.parentId = c.id')
                        ->getDql();
                    $qb->where("NOT EXISTS (" . $db2 . ")");
                    return $qb;
                },
                'group_by' => function($val, $key, $index) {
                    if ($val->getParentCat()) {
                        return $val->getParentCat()->getName();
                    } else {
                        return null;
                    }
                },
            ))
            ->add('status', EntityType::class, array(
                'class' => 'AppBundle:Status',
                'choice_label' => 'name',
            ))
            ->add('completed', null, array(
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy HH:mm:ss',
            ))
            ->add('text', TextareaType::class)
            ->add('result', TextareaType::class)
            ->add('price', NumberType::class)
            ->add('attachments', CollectionType::class, array(
                'entry_type'   => AttachmentType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'entry_options'  => array(
                    'required'  => false,
                    'attr'      => array('class' => 'attachment-box')
                ),
            ))
            ->add('save', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-success btn-md btn-block'
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'compound'     => true,
            'data_class'         => 'AppBundle\Entity\OrderItem',
            'translation_domain' => 'messages',
            'csrf_protection'    => true,
            'csrf_token_id'      => 'order_item',
        ));
    }
}
