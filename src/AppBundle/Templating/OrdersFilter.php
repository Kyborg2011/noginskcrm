<?php
namespace AppBundle\Templating;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

class OrdersFilter
{
    protected $requestStack;
    protected $em;

    public function __construct (EntityManager $em, RequestStack $requestStack) {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public function show ()
    {
        $request = $this->requestStack->getCurrentRequest();
        $statuses = $this->em->getRepository('AppBundle:Status')->findAll();
        $categories = $this->em->getRepository('AppBundle:Category')->findAll();

        return array(
            'statuses' => $statuses,
            'categories' => $categories,
            'request_data' => $request->query->all(),
        );
    }
}
