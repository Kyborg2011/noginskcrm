<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parentId", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="category")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parentCat")
     */
    private $childrenCats;


    public function __construct() {
        $this->orders = new ArrayCollection();
        $this->childrenCats = new ArrayCollection();
    }

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="childrenCats")
     * @ORM\JoinColumn(name="parentId", referencedColumnName="id")
     */
    protected $parentCat;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     *
     * @return Category
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\OrderItem $order
     *
     * @return Category
     */
    public function addOrder(\AppBundle\Entity\OrderItem $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\OrderItem $order
     */
    public function removeOrder(\AppBundle\Entity\OrderItem $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set childrenCats
     *
     * @param \AppBundle\Entity\Category $childrenCats
     *
     * @return Category
     */
    public function setChildrenCats(\AppBundle\Entity\Category $childrenCats = null)
    {
        $this->childrenCats = $childrenCats;

        return $this;
    }

    /**
     * Get childrenCats
     *
     * @return \AppBundle\Entity\Category
     */
    public function getChildrenCats()
    {
        return $this->childrenCats;
    }

    /**
     * Add childrenCat
     *
     * @param \AppBundle\Entity\Category $childrenCat
     *
     * @return Category
     */
    public function addChildrenCat(\AppBundle\Entity\Category $childrenCat)
    {
        $this->childrenCats[] = $childrenCat;

        return $this;
    }

    /**
     * Remove childrenCat
     *
     * @param \AppBundle\Entity\Category $childrenCat
     */
    public function removeChildrenCat(\AppBundle\Entity\Category $childrenCat)
    {
        $this->childrenCats->removeElement($childrenCat);
    }

    /**
     * Set parentCat
     *
     * @param \AppBundle\Entity\Category $parentCat
     *
     * @return Category
     */
    public function setParentCat(\AppBundle\Entity\Category $parentCat = null)
    {
        $this->parentCat = $parentCat;

        return $this;
    }

    /**
     * Get parentCat
     *
     * @return \AppBundle\Entity\Category
     */
    public function getParentCat()
    {
        return $this->parentCat;
    }
}
