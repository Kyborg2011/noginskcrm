<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Applicant
 *
 * @ORM\Table(name="applicant")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicantRepository") @ORM\HasLifecycleCallbacks
 */
class Applicant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="house", type="string", length=5, nullable=true)
     */
    private $house;

    /**
     * @var int
     *
     * @ORM\Column(name="appartment", type="smallint", nullable=true)
     */
    private $appartment;

    /**
     * @var string
     *
     * @ORM\Column(name="holder", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $holder;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="applicant", orphanRemoval=true)
     */
    private $orders;

    public function __construct() {
        $this->orders = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Applicant
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set house
     *
     * @param string $house
     *
     * @return Applicant
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set appartment
     *
     * @param integer $appartment
     *
     * @return Applicant
     */
    public function setAppartment($appartment)
    {
        $this->appartment = $appartment;

        return $this;
    }

    /**
     * Get appartment
     *
     * @return int
     */
    public function getAppartment()
    {
        return $this->appartment;
    }

    /**
     * Set holder
     *
     * @param string $holder
     *
     * @return Applicant
     */
    public function setHolder($holder)
    {
        $this->holder = $holder;

        return $this;
    }

    /**
     * Get holder
     *
     * @return string
     */
    public function getHolder()
    {
        return $this->holder;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Applicant
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\OrderItem $order
     *
     * @return Applicant
     */
    public function addOrder(\AppBundle\Entity\OrderItem $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\OrderItem $order
     */
    public function removeOrder(\AppBundle\Entity\OrderItem $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
