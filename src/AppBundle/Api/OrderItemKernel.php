<?php
namespace AppBundle\Api;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

use AppBundle\Entity\OrderItem;

class OrderItemKernel
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function remove($orderId) {
        $orderItem = $this->em->getRepository('AppBundle:OrderItem')
            ->find($orderId);

        if ($orderItem) {
            $this->em->remove($orderItem);
            $this->em->flush();
        }
    }
}
