<?php
namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

use \Yubikey\Validate; // Yubico OTP key validate utility

class YubikeyAuthenticator extends AbstractGuardAuthenticator
{
    private $em;
    private $apiKey;
    private $clientId;

    /*
     * $apiKey, $clientId - yubico api settings from official site.
     * $apiKey, $clientId are setted in parameters.yml
     */
    public function __construct(EntityManager $em, $apiKey, $clientId)
    {
        $this->em = $em;
        $this->apiKey = $apiKey;
        $this->clientId = $clientId;
    }

    public function getCredentials(Request $request)
    {
        /*
         * If login form isn't sended - any authentication is stopped
         */
        if (!$request->request->get('authenticate_action', 0)) {
            return;
        }
        $yubikey = $request->request->get('yubikey');
        $username = $request->request->get('_username');
        $password = $request->request->get('_password');
        return array(
            'yubikey' => $yubikey,
            'username' => $username,
            'password' => $password,
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $yubikey = $credentials['yubikey'];
        $username = $credentials['username'];
        $user = null;

        if ($yubikey) {

            /*
             *  Yubico OTP can't contain less then 32 symbols
             *  or more then 48 symbols
             */
            if (strlen($yubikey) < 32 || strlen($yubikey) > 48) {
                return null;
            }

            /*
             *  Getting user with current yubico identity (first 12 symbols of OTP key)
             */
            $user = $this->em->getRepository('AppBundle:User')
                ->findOneBy(array(
                    'yubikeyIdentity' => substr($yubikey, 0, 12),
                ));
        }
        return $user;
    }

    /*
     * User is authenticated if OTP key is validated
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $result = false;
        $yubikey = $credentials['yubikey'];
        if ($yubikey) {
            $v = new Validate($this->apiKey, $this->clientId);
            $response = $v->check($yubikey);
            if ($response->success() === true) {
                $result = true;
            }
        }
        return $result;
    }

    /*
     * If user authenticated - redirect to homepage (crm's order list)
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse('/');
    }

    /*
     * If user isn't authenticated - put error string in session
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $error_message = "Yubikey is entered incorrectly";
        if ($request->request->get('yubikey')) {
            $request->getSession()->set("yubikey_error", $error_message);
        }
        return null;
    }

    /*
     * Calling when authentication is needed, but Yubico key have not sended yet
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return $request;
    }

    /*
     * "Remember me" function is currently dasabled
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
