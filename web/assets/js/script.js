$(document).ready(function() {

    /* Jquery dateTimePicker initialization */
    var dateTimeInputs = $('#order_item_completed, #order_item_accepted');
    if (dateTimeInputs.length) {
        dateTimeInputs.datetimepicker({
	        timeInput: true,
	        timeFormat: "HH:mm:ss",
            onSelect: function(date, inst) {
                var form = $('form[name="order_item"]');
                form.formValidation('revalidateField', 'order_item[accepted]');
                form.formValidation('revalidateField', 'order_item[completed]');
            }
        });
    }

    $('.data-input').datepicker();

    /* Form validation plugin init */
    $('form[name="order_item"]').formValidation({
        framework: 'bootstrap',
        locale: 'ru_RU',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
    });

    /* Delete request modal */
    $('#orderDeleteModal').on('show.bs.modal', function (event) {
        window.order_delete_button = $(event.relatedTarget)
        window.order_delete_href = window.order_delete_button.data('order-href')
    })

    $('.btn-remove-order').click(function(e) {
        $.ajax({
            method: "GET",
            url: window.order_delete_href,
            dataType: 'json'
        })
        .done(function(msg) {
            $('#orderDeleteModal').modal('hide');
            if (msg.message) {
                $('.ajax-notification').text('Заявка успешно удалена')
                $('.ajax-notification').show()
                window.order_delete_button.closest('tr').hide();
            }
        })
        return false;
    })

    $('#side-menu').metisMenu({
        toggle: false
    });

    var orders_filter = $('#orders-filter-form');
    if (orders_filter.length) {
        $('#orders-filter-form li > a').click(function(){
            var checkbox = $(this).find('input[type=checkbox]');
            var current_li = $(this).closest('li');
            if (!current_li.find('ul').length) {
                if (checkbox.is(":checked")) {
                    checkbox.prop('checked', false);
                    $(this).removeClass('checked-value');
                } else {
                    checkbox.prop('checked', true);
                    $(this).addClass('checked-value');
                }
            }
            return false;
        });
    }

    /* Bootstrap select init */
    $('.selectpicker').selectpicker({
        style: 'btn-default',
        size: 4
    });



});
